# Description
A tidier demo based on article by Valentino Gagliardi&rsquo;s introduction to ReactJS with Redux.

## Quick word about babel-presets
During compilation I was warned about **env** and to visit the [babel homepage](http://babeljs.io/env "link to env advisory") to read further on the subject. Although, I did not utilize **babel-preset-es2015** specifically, it is safe to say that **babel-preset-env** is going to be shelved in favor of **&#64;babel/preset-env**. You can see the changes in the commit history if you desire. Also, you may as well go for the gusto in this case. In the short run, you would run something to this effect to swap out one package for the other like so:
```sh
$ npm uninstall babel-preset-env babel-core babel-loader babel-preset-react transform-object-rest-spread --save-dev
$ npm install @babel/preset-env @babel/core "babel-loader@^8.0.0-beta" @babel/preset-react @babel/plugin-proposal-object-rest-spread --save-dev
```

## Please visit
You can see [the lesson](https://artavia.gitlab.io/redux_intro_v16/dist/index.html "the lesson at github") and decide if this is something for you to play around with at a later date. I have composed an accompanying repository that deals with [ReactJS versions less than 16](https://www.github.com/donlucho/redux_intro_v15/ "link to companion repo"). 

## Some Additional Comments
Other than the final product, I have included the previous phases in the app.js file (Max calls it index.js in case you had not noticed) that went into comprehending the subject of Redux before tackling ReactJS with Redux, per se. At your leisure, you can either waste &lsquo;em or pore over them for your own benefit. If you are of a rare breed that does not need to warm&#45;up before any exercise, then, have at it! Also, rather than maintaining a standalone .babelrc JSON file, I included the additional parameters in the webpack.config.js file necessary to make this thing run, too.

## Main Sources
In addition to consulting Valentino&rsquo;s article entitled [React Redux Tutorial for Beginners: learning Redux in 2018](https://www.valentinog.com/blog/react-redux-tutorial-beginners/ "link to React Redux Tutorial for Beginners, et. al."), I decided it was necessary to cure the dilemma of a previously enabled submit button. For this part of the lesson, I found an article entitled [Form recipe: Conditionally disabling the Submit button](https://goshakkk.name/form-recipe-disable-submit-button-react/ "link to Form recipe: Conditionally disabling the Submit button") to also be of great help. PEACE.

### My name is Luis
You can call me don Lucho &amp; I hope you have fun during the rest of your day building something cool to share with the rest of the world!