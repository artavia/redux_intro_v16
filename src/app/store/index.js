import { createStore } from 'redux';
import {rootReducer} from '../reducers/index';
const store = createStore(rootReducer);

var fatfunk = () => { console.log( 'fatmethod ~ Store updated: ' , store.getState() );  } ;
store.subscribe( fatfunk );

export {store};