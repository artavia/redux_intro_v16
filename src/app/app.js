import React from 'react';
import { render } from 'react-dom'; 

import { Provider } from 'react-redux';
import {store} from "./store/index";

// import { CustomApp } from './containers/a';
// import { CustomApp } from './containers/b';
import { CustomApp } from './containers/c';

render(
  <Provider store={store}>
    <CustomApp/>
  </Provider> 
  , document.querySelector( '#leApp' ) 
);