import { ADD_ARTICLE } from '../constants/action-types';

const addArticle = (article) => {
  return {
    type: ADD_ARTICLE
    , payload: article
  };
};

export {addArticle};