
// Two key points for avoiding mutations to reference types in Redux
  // for arrays, use concat(), slice(), and ...spread
  // for objects, use Object.assign() and ...spread

import { ADD_ARTICLE } from '../constants/action-types';

const initialState = {
  articles: []
};

const rootReducer = (state = initialState, action) => { 
  switch(action.type) {

    case ADD_ARTICLE:

      // !immutable -- boo, hiss!!!
      // state.articles.push(action.payload);
      // return state; 

      // better
      // state = { ...state, articles: state.articles.concat( action.payload ) };

      // best
      state = { ...state , articles: [...state.articles, action.payload ] };
      break;

    default: 
      return state; 
  }

  return state;
};

export {rootReducer};