import React from 'react';
import { connect } from 'react-redux';

const mapStateToProps = (state) => {
  return { articles: state.articles };
};

const ConnectedList = ( {articles} ) => { 
  
  console.log( articles );

  const list = articles.map( (el) => { return (<li key={el.id} className="list-group-item">{el.title}</li>) } );

  return(
    <ul className="list-group list-group-flush">
      {list}
    </ul>
  );
};

const List = connect(mapStateToProps)(ConnectedList);

export {List};