import React from 'react';

const List = (props) => {
  
  const articles = props.myArticles; // console.log( articles );
  const list = articles.map( (el) => { return (<li key={el.id}>{el.title}</li>) } );

  return(
    <div className="row mt-5">
      <div className="col-md-4 offset-md-1">
        <h2>Articles</h2>
        <ul>
          {list}
        </ul>
      </div>
    </div>
  );
};

export {List};