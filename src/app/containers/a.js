
import '../favicon.ico';
import '../css/boots.scss';
import '../js/index.js';

import React from 'react';

class CustomApp extends React.Component {
  
  constructor(){
    super();
    this.state = {
      articles: [
        { title: "React Redux for Beginners", id: 1 }
        , { title: "Redux e React: cos'è Redux e come usarlo con React", id: 2 }
        , { title: "React Redux para novatos y principantes", id: 3 }
      ]
    };
  }

  render(){
    // const articles = this.state.articles;
    const {articles} = this.state; 
    const list = articles.map( (el) => { return (<li key={el.id}>{el.title}</li>) } );
    
    return(
      <div className="container">
        <ul>{list}</ul>
      </div>
    );
  }
}

export {CustomApp};