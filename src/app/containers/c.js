
import '../favicon.ico';
import '../css/boots.scss';
import '../js/index.js';

import React from 'react';

import {List} from '../components/ReduxList';
import { Form } from '../containers/ReduxForm';

class CustomApp extends React.Component {
  
  render(){
    return(
      <div className="container">
        <div className="row mt-5">
          <div className="col-md-4 offset-md-1">
            <h2>Articles</h2>
            <List />
          </div>
          <div className="col-md-4 offset-md-1">
            <h2>Add a new article</h2>
            <Form />
          </div>
        </div>
      </div>
    );
  }
}

export {CustomApp};