import React from 'react';
import {connect} from 'react-redux';
import uuidv1 from 'uuid'; // import uuid from 'uuid';
import { addArticle } from '../actions/index';

const mapDispatchToProps = (dispatch) => {
  return {
    addArticle: article => dispatch( addArticle(article) )
  };
};

class ConnectedForm extends React.Component {
  
  constructor(){ 
    super(); 
    this.state = { title: "" };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event){
    this.setState( { [event.target.id ]: event.target.value } );
  }

  handleSubmit(event){
    
    if ( !this.canBeSubmitted() ) {
      event.preventDefault();
      return;
    }
    const {title} = this.state;
    const id = uuidv1();
    this.props.addArticle( {title, id } );
    this.setState( {title: "" } );
    event.preventDefault();
  }

  canBeSubmitted(){
    const {title} = this.state;
    return (
      title.length > 0
    );
  }

  render(){
    const {title} = this.state;
    const isEnabled = this.canBeSubmitted();

    return(
      <form onSubmit={this.handleSubmit}>
        <div className="form-group">
          <label htmlFor="title">Title</label>
          <input type="text" placeholder="title of article" className="form-control" id="title" value={title} onChange={this.handleChange}/>
        </div>
        <input type="submit" disabled={!isEnabled} value="Save" className="btn btn-success btn-lg"/>
      </form>
    );
  };

}

const Form = connect( null, mapDispatchToProps )(ConnectedForm);
export {Form};