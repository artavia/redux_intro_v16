
import '../favicon.ico';
import '../css/boots.scss';
import '../js/index.js';

import React from 'react';

import {List} from '../components/List';

class CustomApp extends React.Component {
  
  constructor(){
    super();
    this.state = {
      articles: [
        { title: "React Redux for Beginners", id: 1 }
        , { title: "Redux e React: cos'è Redux e come usarlo con React", id: 2 }
        , { title: "React Redux para novatos y principantes", id: 3 }
      ]
    };
  }

  render(){

    return(
      <div className="container">
        <List myArticles={this.state.articles}/>
      </div>
    );
  }
}

export {CustomApp};